package in.celso.atomys;

import java.awt.*;

import javax.swing.*;

public class TexturePanel extends JPanel {

	private Model model;
	
	private final Color BACKGROUND = new Color(200, 100, 125);
	
	protected TexturePanel(Model model) {
		this.model = model;
		setPreferredSize(new Dimension(model.textureWidth, model.textureHeight));
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(BACKGROUND);
		g.fillRect(0, 0, model.textureWidth, model.textureHeight);
		
        GradientPaint gp = new GradientPaint(
                0, 0, Color.RED,
                model.textureWidth, model.textureHeight, Color.BLUE);

        ((Graphics2D) g).setPaint(gp);
        g.fillRect(0, 0, model.textureWidth, model.textureHeight);
		
		Part root = (Part) model.partsTree.getModel().getRoot();
		
		if (root.children != null && !root.children.isEmpty()) {
			for (int i = 0; i < root.children.size(); i++)
				paintPart(g, root);
		}
	}
	
	private void paintPart(Graphics g, Part part) {
		//draw
		
		if (!part.children.isEmpty()) {
			for (int i = 0; i < part.children.size(); i++)
				paintPart(g, part);
		}
	}

}
