package in.celso.atomys;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

public class Part extends DefaultMutableTreeNode {
	
	public String name;
	public Part parent;
	public List<Part> children;
	public float posX, posY, posZ;
	public float sizeX, sizeY, sizeZ;
	public float rotX, rotY, rotZ;
	public int tOffX, tOffY;
	
	protected Part(String name) {
		this.name = name;
	}
	
	protected Part(String name, Part parent) {
		this.name = name;
		this.parent = parent;
	}

}
