package in.celso.atomys;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

import javax.swing.JMenuItem;

public class CMenuItem extends JMenuItem {
	
	private String meta;

	public CMenuItem(String name, String meta) {
		super(name);
		this.meta = meta;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if (meta == null)
			return;
		
		g.setColor(Color.GRAY);
		
		Rectangle2D bounds = g.getFontMetrics().getStringBounds(meta, g);
		g.drawString(meta, getWidth() - (int) bounds.getWidth(), getHeight() - (int) bounds.getHeight()/2);
		getWidth();
	}
	
	public Dimension getPreferredSize() {
		Dimension dim = super.getPreferredSize();
		if (meta != null)
			dim.width += 10 + getFontMetrics(getFont()).stringWidth(meta);
		return dim;
	}
	
}
