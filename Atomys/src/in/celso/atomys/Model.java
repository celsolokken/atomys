package in.celso.atomys;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

public class Model {
	
	protected String name;
	protected String version;
	
	protected JTree partsTree;
	protected int textureWidth = 128, textureHeight = 64;
	
	protected Model(String name, String version) {
		partsTree = new JTree(new Part(name));
	}
	
	protected void addPart(Part part) {
		addPart(part, (Part) partsTree.getModel().getRoot());
	}
	
	protected void addPart(Part part, Part parent) {
		List<Part> children = parent.children;
		
		if (children == null)
			children = new ArrayList<>();
		
		children.add(part);
		
		((DefaultTreeModel) partsTree.getModel()).reload((TreeNode) partsTree.getModel().getRoot());
		partsTree.revalidate();
		partsTree.repaint();
	}
	
	protected void getAllParts() {
		List<Part> rootChildren = ((Part) partsTree.getModel().getRoot()).children;
		
		assert rootChildren != null;
		
		//for (Part p : )
	}
}
